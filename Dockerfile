FROM openjdk:8-alpine
COPY /target/Api-Investimentos-*.jar api-Investimentos-jar 
CMD ["java","-jar", "api-Investimentos-jar"]
